<?php

namespace App\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Category;

/**
 * @Route("admin/categories", name="categories")
 */
class CategoriesController extends Controller
{

	/**
	 * @Route("/view", name="View")
	 * @Security("has_role('ROLE_ADMIN')")
	 */
	public function viewAction(){
		$em=$this->getDoctrine()->getManager();
    	$repository=$em->getRepository(Category::class);
    	$categories=$repository->findAll();
    	return $this->render('back/categories.html.twig',['categories'=>$categories]);
	}

	/**
	 * @Route("/create", name="Create")
	 * @Security("has_role('ROLE_ADMIN')")
	 */
    public function createAction(Request $request)
    {
        $em=$this->getDoctrine()->getManager();

        $category=new Category();

        /**
        * @var Form
        */
        $form=$this->createForm(\App\Form\CategoryType::class,$category);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
        	$em->persist($category);
        	$em->flush();

        }
        return $this->render('back/new_category.html.twig',['form'=>$form->createView()]);
    }



    /**
	 * @Route("/delete/{id}", name="Delete")
	 * @Security("has_role('ROLE_ADMIN')")
	 */
    public function deleteAction($id=0){
    	$em = $this->getDoctrine()->getManager();
	    $item=$em->getRepository(Category::class)->find($id);
	    $em->remove($item);
	    $em->flush();
	    $this->addFlash('notice','Deleted Successfully!');

	    return $this->redirectToRoute('categoriesView');
    }


}
