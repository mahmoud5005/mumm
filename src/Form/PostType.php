<?php

namespace App\Form;

use App\Entity\Post;
use App\Entity\Category;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class PostType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $categories = array();

        $builder
            ->add('title')
            ->add('article')
            ->add('category',EntityType::class,array(
                'class'       => Category::class,
                'placeholder' => '',
                'choice_label' => 'title',
            ))
            ->add('image',Type\FileType::class)

            ->add('Save',Type\SubmitType::class)
        ;

    }



    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            // uncomment if you want to bind to a class
            //'data_class' => Post::class,
        ]);
    }
}
