<?php

namespace App\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Post;
use App\Entity\Comments;
use App\Entity\Category;


class HomeController extends Controller
{
    /**
     * @Route("/", name="home")
     * @Route("/{id}" , requirements={"id" = "\d+"})
     */
    public function index($id=0)
    {
        $em=$this->getDoctrine()->getManager();
    	$postsRepository=$em->getRepository(Post::class);
    	$posts=$postsRepository->findByPublish();
		
		if($id!=0){
    		$posts=$postsRepository->findByCategory($id);
    	}else{
    		$posts=$postsRepository->findByPublish();
    	}
    	$catsRepository=$em->getRepository(Category::class);
    	$categories=$catsRepository->findAll();
    	

        return $this->render('front/posts.html.twig',['posts'=>$posts,'categories'=>$categories]);
    }


    /**
     * @Route("/post/{id}", name="view_post")
     */
    public function viewPost($id=0,Request $request)
    {
        $em=$this->getDoctrine()->getManager();
    	$postRepository=$em->getRepository(Post::class);
    	$post=$postRepository->find($id);

    	$comments=$post->getComments();

    	if($request->isMethod('post')){
    		$comment=new Comments();
    		$comment->setPost($post);
    		$comment->setComment($request->get('comment'));
    		$comment->setSubject($request->get('subject'));
    		$em->persist($comment);
        	$em->flush();
    	}

    	$catsRepository=$em->getRepository(Category::class);
    	$categories=$catsRepository->findAll();

        return $this->render('front/post.html.twig',['post'=>$post,'comments'=>$comments,'categories'=>$categories]);
    }
}
