<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;


/**
 * @ORM\Entity(repositoryClass="App\Repository\PostRepository")
 */
class Post
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
    * @ORM\Column(type="string",name="title")
    * @var string
    */
    private $title;


    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Category", inversedBy="post")
     * @ORM\JoinColumn(nullable=true)
     */
    private $category;


    /**
    * @ORM\Column(type="text",name="article")
    * @var string
    */
    private $article;

    /**
    * @ORM\Column(type="boolean",name="published")
    * @var string
    */
    private $published;


    /**
    * @ORM\Column(type="string",name="image")
    * @Assert\NotBlank(message="Please Insert Image")
    * @Assert\File(mimeTypes={"image/jpeg","image/png"})
    */
    private $image;



    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Comments", mappedBy="post")
     */
    private $comments;

    public function __construct()
    {
        $this->comments = new ArrayCollection();
    }

    /**
     * @return Collection|Comments[]
     */
    public function getComments()
    {
        return $this->comments;
    }


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     *
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     *
     * @return self
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return string
     */
    public function getArticle()
    {
        return $this->article;
    }

    /**
     * @param string $article
     *
     * @return self
     */
    public function setArticle($article)
    {
        $this->article = $article;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param mixed $image
     *
     * @return self
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * @return string
     */
    public function getPublished()
    {
        return $this->published;
    }

    public function getCategory(): ?Category
    {
        return $this->category;
    }

    public function setCategory(Category $category=null)
    {
        $this->category = $category;
    }

    /**
     * @param string $published
     *
     * @return self
     */
    public function setPublished($published)
    {
        $this->published = $published;
        return $this;
    }
}
