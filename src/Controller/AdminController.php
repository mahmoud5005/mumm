<?php

namespace App\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\HttpFoundation\Session\Session;

/**
 * @Route("/admin", name="admin")
 */
class AdminController extends Controller
{
    
    /**
     * @Route("/", name="")
     */
    public function index()
    {
        return $this->redirectToRoute('categoriesView');
    }

    /**
     * @Route("/login", name="login")
     */
    public function login(Request $request, AuthenticationUtils $authUtils)
	{
	    // get the login error if there is one
	    $error = $authUtils->getLastAuthenticationError();

	    // last username entered by the user
	    $lastUsername = $authUtils->getLastUsername();

	    return $this->render('security/login.html.twig', array(
	        'last_username' => $lastUsername,
	        'error'         => $error,
	    ));
	}


	/**
     * @Route("/logout", name="Logout")
     */
    public function logout(Request $request, AuthenticationUtils $authUtils)
	{
	    $session = new Session();

	    $session->invalidate();
	 	
	 	return $this->redirectToRoute('categoriesView');   

	}

}
