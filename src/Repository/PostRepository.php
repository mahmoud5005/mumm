<?php

namespace App\Repository;

use App\Entity\Post;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

class PostRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Post::class);
    }

    
    public function findByCategory($value)
    {
        return $this->createQueryBuilder('p')
            ->where('p.category = :value')->setParameter('value', $value)
            ->andWhere('p.published = :v')->setParameter('v', 1)
            ->orderBy('p.id', 'ASC')
            ->getQuery()
            ->getResult()
        ;
    }

    public function findByPublish($value=1)
    {
        return $this->createQueryBuilder('p')
            ->where('p.published = :value')->setParameter('value', $value)
            ->orderBy('p.id', 'ASC')
            ->getQuery()
            ->getResult()
        ;
    }
    
}
