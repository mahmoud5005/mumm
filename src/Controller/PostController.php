<?php

namespace App\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Post;


/**
 * Class PostController
 * @package App\Controller
 * @Route("admin/post",name="post")
 */
class PostController extends Controller
{
   
    /**
     * @Route("/", name="index")
     * @Security("has_role('ROLE_ADMIN')")
     */ 
    public function indexAction()
    {
    	$em=$this->getDoctrine()->getManager();
    	$repository=$em->getRepository(Post::class);
    	$posts=$repository->findAll();


        return $this->render('post/index.html.twig',['posts'=>$posts]);
    }






    /**
     * @Route("/view", name="View")
     * @Security("has_role('ROLE_ADMIN')")
     */ 
    public function viewAction()
    {
        $em=$this->getDoctrine()->getManager();
        $repository=$em->getRepository(Post::class);
        $posts=$repository->findAll();
        return $this->render('back/posts.html.twig',['posts'=>$posts]);
    }



    /**
     * @Route("/create", name="Create")
     * @Security("has_role('ROLE_ADMIN')")
     */ 
    public function createAction(Request $request)
    {
        $em=$this->getDoctrine()->getManager();

        $post=new Post();

        /**
        * @var Form
        */
        $form=$this->createForm(\App\Form\PostType::class,$post);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){

        	/**
        	 * @var UploadedFiel $file
        	 */
        	$file=$post->getImage();
        	$fileName=md5(uniqid()).'.'.$file->guessExtension();

        	$file->move(
        		$this->getParameter('images_directory'),
        		$fileName
        	);

        	$post->setImage($fileName);
            $post->setPublished(0);
        	$em->persist($post);
        	$em->flush();

        }
        return $this->render('back/new_post.html.twig',['form'=>$form->createView()]);
    }

    /**
     * @Route("/delete/{id}", name="Delete")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function deleteAction($id=0){
        $em = $this->getDoctrine()->getManager();
        $item=$em->getRepository(Post::class)->find($id);
        $em->remove($item);
        $em->flush();
        $this->addFlash('notice','Deleted Successfully!');

        return $this->redirectToRoute('categoriesView');
    }

    /**
     * @Route("/publish/{id}", name="Publish")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function publishAction($id=0){
        $em = $this->getDoctrine()->getManager();
        $post = $em->getRepository(Post::class)->find($id);

        if (!$post) {
            throw $this->createNotFoundException(
                'No Posts found for id '.$id
            );
        }

        $post->setPublished(1);
        $em->flush();

        return $this->redirectToRoute('postView');
    }

    /**
     * @Route("/unpublish/{id}", name="Unpublish")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function unpublishAction($id=0){
        $em = $this->getDoctrine()->getManager();
        $post = $em->getRepository(Post::class)->find($id);

        if (!$post) {
            throw $this->createNotFoundException(
                'No Posts found for id '.$id
            );
        }

        $post->setPublished(0);
        $em->flush();

        return $this->redirectToRoute('postView');
    }
}
